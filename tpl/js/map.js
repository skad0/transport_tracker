ymaps.ready(init);

var map;

function init() {
    map = new ymaps.Map('map', {
        center: [44.60681174917277, 33.5837725048828],
        zoom: 12,
        controls: ['zoomControl']
    });
}
