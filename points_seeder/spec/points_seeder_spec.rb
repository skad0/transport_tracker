require 'rspec'
require 'spec_helper'
require_relative '../points_seeder'

RSpec.describe PointsSeeder do

  let(:url) { 'http://localhost:3900' }
  let(:data) do 
    [
      {:latitude => 1.0, :longtitude => 2.0},
      {:latitude => 3.2, :longtitude => 4.1}
    ] 
  end
  let(:item) { data.first }
  let(:seeder) { PointsSeeder.new(url, data) }

  describe "#new" do
    it "returns new PointsSeeder object" do
      expect(seeder).to be_a PointsSeeder
    end
  end

  describe "#take_random_point" do
    it "returns random coordinates pair" do
      coordinates = seeder.take_random_point
      expect(coordinates).to be_a(Hash).and include(:latitude, :longtitude)
    end
  end

  describe "#take_random_coordinate" do
    it "returns random coordinate" do
      coordinate = seeder.take_random_coordinate
      expect(coordinate).to be_a(Float).and be_between(0, 180)
    end
  end

  # @todo think about network error and retry
  describe "#send_item" do
    it "send request to server with specified item" do
      allow(Net::HTTP).to receive(:post_form)
      expect(Net::HTTP).to receive(:post_form).with(URI.parse(url), item)
      seeder.send_item(item)
    end
  end

  describe "#send_data" do
    it "send all data to the server" do
      allow(seeder).to receive(:send_item)
      expect(seeder).to receive(:send_item).with(hash_including(:latitude, :longtitude))
      seeder.send_data
    end 
  end

end