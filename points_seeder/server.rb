require 'socket'
require './points_seeder'

server = TCPServer.new('localhost', 3300)

loop do

  socket = server.accept

  request = socket.gets

  seeder = PointsSeeder.new(
    'http://localhost:3000/',
    [
      {:latitude => 1.0, :longtitude => 2.0},
      {:latitude => 2.1, :longtitude => 2.6}
    ]
  )

  seeder.send_data

  STDERR.puts request

  response = "Hello World!\n"

  socket.print "HTTP/1.1 200 OK\r\n" +
               "Content-Type: text/plain\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"

  socket.print "\r\n"
  socket.print response
  socket.close
end