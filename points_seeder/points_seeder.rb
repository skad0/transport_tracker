require "net/http"
require "uri"

class PointsSeeder
	attr_accessor :data

  def initialize(url, data, delay = 1)
  	@uri = URI.parse(url)
  	@data = data
  	@delay = delay
  end

  def take_random_point
    {
      :latitude => take_random_coordinate,
      :longtitude => take_random_coordinate
    }
  end

  def take_random_coordinate
    Random.rand(0.0...180.0)
  end

  def send_item(item)
  	Net::HTTP.post_form(@uri, item)
  end

  def send_data
  	@data.each do |item|
  		send_item item
  		sleep @delay
  	end
  end
end